---
author: Antoni Mazurczak
title: Ulubiona książka 
subtitle: Eragon
date: 07.11.2021
theme: Poland
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}

---


 ## Slajd nr 1 - Prezentacja Książki  

**Eragon** jest powieścią Christophera Paoliniego pochodzącego z USA stanowiąca pierwszą część cyklu Dziedzictwo. Została opublikowana w 2003 roku przez wydawnictwo Alfred A. Knopf, w Polsce przez Mag. Znajdowała się na liście bestsellerów New York Timesa

![](C:\Users\Ja\zad2\okladka.jpg)















## Slajd nr 2 - Przedstawienie bochaterów



1.  **Eragon Bromsson**, znany również jako Cieniobójca (po zwycięstwie nad cieniem Durzą), Argetlam (jako Smoczy Jeździec), Ognisty Miecz (jako właściciel miecza Brisingr) oraz Królobójca (po zwycięstwie nad Galbatorixem) — główny bohater i protagonista serii Dziedzictwo. Jest synem Jeźdźca Broma i Seleny. Przez pierwszych piętnaście lat życia Eragon był wieśniakiem, mieszkającym i pracującym na farmie swojego wuja Garrowa. Trudził się nie tylko uprawą roli, lecz również polowaniami, zapewniając tym samym wyżywienie swojej rodzinie. Podczas jednego z polowań natknął się w lesie na błękitny kamień, który okazał się być smoczym jajem.

   ![](C:\Users\Ja\zad2\eragon.jpg)

2. ​	**Brom** - potężny Jeździec, był założycielem Vardenów i ojcem Eragona Cieniobójcy. Po upadku Jeźdźców życie swe poświęcił zwalczaniu Galbatorixa i jego sług. Zabił kilkoro Zaprzysiężonych i był zamieszany w śmierć kolejnych kilku. Jednak największą sławę przyniosło mu zabicie Morzana, swego niegdysiejszego przyjaciela. To Brom nauczył Eragona fechtunku i podstaw magii oraz przeprowadził chłopaka przez Tuatha du orothrim

   ![](C:\Users\Ja\zad2\Brom.jpg)

3. ​	**Arya Dröttning**, zwana też Cieniobójczynią (po pokonaniu Varauga) − elfka będąca jedną z głównych bohaterek cyklu Dziedzictwo, córka królowej Islanzadí i króla Evandara, który poległ w bitwie, gdy Arya była jeszcze młoda. Jest Smoczym Jeźdźcem związanym z Fírnenem. Po zakończeniu wojny z Galbatorixem została wybrana na królową elfów.

   ![](C:\Users\Ja\zad2\Arya.jpg)

4. ​	**Roran Garrowsson Młotoręki**- jedyny potomek Garrowa i Marian, kuzyn Jeźdźca Eragona Cieniobójcy, mąż Katriny oraz ojciec Ismiry, vardeński dowódca. Pochodził z Carvahall, którego mieszkańców wyprowadził do Surdy po zniszczeniu wioski przez żołnierzy Imperium.

![](C:\Users\Ja\zad2\Roran.jpg)





## Slajd nr 3 - 4 Księgi

### Cykl Dziedzictwo 
#### Eragon

#### Najstarszy

#### Brisingr

#### Dziedzictwo 

## Slajd 4 -  Słów kilka o Christopher Paolini

**Christopher Paolini** urodzony 17 listopada 1983 w Paradise Valley – amerykański pisarz pochodzenia włoskiego, autor cyklu Dziedzictwo.

​	Uczył się w domu i szkołę średnią ukończył w wieku 15 lat dzięki kursom korespondencyjnym. To właśnie wtedy zaczął pracować nad powieścią Eragon i dwiema następnymi. W ciągu pierwszego roku powstał zarys książki, następny rok autor poświęcił na jej rozbudowanie. W 2002 r. Eragon został wydany przez Paolini International LCC, spółkę wydawniczą rodziców Paoliniego. Następnie autor odwiedził 130 szkół, dyskutując na temat książki, czytając i pisząc. Kiedy powieściopisarz Carl Hiaasen za namową swojego syna przeczytał książkę, zawiadomił swojego wydawcę, Knopfa. Po większych korektach, gdy około 20 000 słów zostało wyciętych, druga edycja Eragona została opublikowana w sierpniu 2003 r.

​	Twórczość Paoliniego traktuje się z rosnącym dystansem – z jednej strony cichnie szum medialny zarówno wokół jego osoby, jak i jego książek, z drugiej strony powszechnie wiadomym faktem jest, że znaczącą rolę w promocji książki odegrali rodzice autora oraz firma wydawnicza będąca ich własnością, co również spotyka się z krytyką.

31 grudnia 2018 ukazał się zbiór opowiadań The Fork, the Witch, and the Worm, osadzony w Alagaësii, świecie Eragona. Fragment do książki napisała Angela Paolini, siostra Christophera.



## Slajd 5 - Sukcesy i porażki książki

​	**Eragon** figurował 24 tygodnie na liście bestsellerów; tylko piąta część przygód Harry’ego Pottera sprzedawała się lepiej, zaś wokół samej książki zgromadziło się wielu fanów. Eragon stał się także przyczyną dyskusji na temat homeschoolingu, ale sama jakość książki też stała się powodem do debat. Początkowo część krytyków nazwała Paoliniego „objawieniem”, zachwycając się wykreowaną przez niego krainą. Kiedy jednak blask nowości osłabł, przebiły się inne głosy, zwracające uwagę na to, że powieść jest jedynie komercyjnym produktem, w którym brak jakiejkolwiek oryginalności, postacie są bezlitośnie płaskie, a fabuła łatwa do przewidzenia.

## Slajd 6 - Opinie o książce 



#### Recenzuje: **valeska**

Zaczynając myśleć o tej książce, chyba najbardziej początkowo skupiałam się na pytaniu: co tak naprawdę potrzebne jest do sukcesu powieści? W tym przypadku? Oczywiście niesamowita historia. I stwierdzenie to nie dotyczy jedynie treści „Eragona”. Dotyczy także samego faktu i sposobu, w jaki ona zaistniała. 

Kolejnym pytaniem było to, co jest bardziej z tych dwóch rzeczy fascynujące dla ludzi, którzy promują powieść fantasy Christophera Paoliniego. Nie da się ukryć, iż fakt, że to piętnastolatek stworzył takie dzieło oraz to, że jego rodzice włożyli wiele wysiłku w początkowych fazach jej „zaistnienia” jest doskonałym chwytem. A treść? Zostaje gdzieś na drugim planie. Historia smoka i jego jeźdźca przegrała z historią autora? Co może zrobić piętnastolatek, który do tej pory jedynie słyszał szczątki legend o smokach i ich jeźdźcach, a teraz nagle sam staje się właścicielem jaja, a w końcu… smoka, jeśli nie zaopiekować się nim? Oczywiście jako jedyny na świecie, jakby się wydawało, właściciel smoka nie może być pozbawiony szeregu przygód. 

Z tego samego powodu ściąga na siebie uwagę tych „niedobrych”, którzy depczą im po piętach, tworząc oczywiście niezastąpiony motyw wędrówki- ucieczki [?]. A ucieczek jest tu sporo, zwłaszcza biorąc pod uwag? niezbyt przyjaznych Urgali, którzy depczą im po piętach. Jako że Saphira została przeznaczona młodemu Eragonowi, para ta tworzy zgrany duet. Smoczyca wiernie postanawia pomóc Jeźdźcowi w zemście za śmierć wuja. Paolini jest osobą wychowaną na literaturze typu fantasy. Zafascynowany nią stworzył swoje dzieło wydawać by się mogło posługując się znanymi sobie schematami. Momentami ma się wrażenie, że główny bohater jest postacią bez ikry. Zawsze ktoś inny wie „coś” lepiej od niego i mówi mu, co ma zrobić. Właściwie to nic dziwnego, w końcu do tej pory wiódł nudne życie i nagle znalazł się w tak innej sytuacji. 

Oczekiwałabym jednak w kolejnych tomach jego większej indywidualności. „Eragon” to z pewnością jedna z tych książek, do których przeczytania nie trzeba być fanem fantasy, mimo iż ta właśnie taki gatunek reprezentuje. Nie przesadzę mówiąc, że może przeczytać ją każdy, od tak, bajka do poduszki.





#### Recenzjuje: **@Czekoladowaa**

Christopher Paolini urodził się w 1983 roku. Jest amerykańskim pisarzem włoskiego pochodzenia. Powieść "Eragon" zaczął pisać w wieku 15 lat. Wkrótce potem książka została wydana przez wydawnictwo jego rodziców. Książka młodego autora została przyjęta z wielkim entuzjazmem. Kiedy jednak blask nowości osłabł, przebiły się inne głosy, zwracające uwagę na to, że powieść jest jedynie komercyjnym produktem, w którym brak jakiejkolwiek oryginalności, postacie są bezlitośnie płaskie, a fabuła łatwa do przewidzenia.

Eragon - prosty chłopak znajduje niezwykły kamień. Wkrótce wykluwa się z niego smoczyca. Tymczasem na wioskę napadają nieludzkie stworzenia, które zabijają jedynego opiekuna Eragona - Garrowa. Chłopiec chce odszukać morderców wuja i ich zabić. Wyrusza w niebezpieczną podróż razem z miejscowym bajarzem - Bromem. Jak potoczą się ich losy?

O tej książce słyszałam bardzo wiele. Niektórzy mówią, że jest to świetna powieść o wspaniałej fabule. Reszta uważa, że jest to bardzo denna książka pozbawiona większego sensu. Bardzo dużo osób też mówi, że młody autor wymyślił dość prostą fabułę i po prostu resztę faktów "ściągnął" z innych wspaniałych powieści fantasty. A jak ja uważam? Fabuła faktycznie nie jest zbytnio rozbudowana - nie tak jak oczekiwałam po tak znanej książce. Uważam, ze bohaterowie są mało barwni, a ich charaktery są znane z innych powieści fantasty.

Na początku książki język jakim została napisana książka wydawał mi się bardzo skomplikowany. Długie złożone zdania bardzo utrudniały mi czytanie. Odczułam to tak jakby autor chciał na siłę udowodnić, że w tak młodym wieku można pisać książki prostym, zrozumiałym językiem. Nie wiem czy to wina autora, czy tłumacza, ale na pewno początki czytania nie należały do tych najlepszych. Później na szczęście wczułam się w książkę i czytanie sprawiało mi mniejsze problemy. Czasami książka potrafiła mnie także rozbawić. Eragon na końcu prawie wszystkich rozdziału mdlał, tracił tracił przytomność lub zasypiał. Tak jakby autor nie potrafił już nic lepszego wymyślić.

"Eragon" to powieść napisana bez większego sensu. Książka nie posiada żadnego morału. Nic dobrego nie wnosi do naszego życia. Oczywiście istniej coś takiego jak zwycięstwo dobra nad złem, ale to chyba występuje w każdej książce fantasty. Tak więc nie jest to książka mądra i niczego mnie nie nauczyła.
Należy również wspomnieć o wydaniu książki. Na okładce widnieje smok. Tytułowy? Nie. Książka nosi nazwę "Eragon", czyli chłopaka, który znalazł jajo. Według mnie to duży błąd. Czytelnik, który pierwszy raz widzi tą książkę od razu sądzi, że to smok nazywa się Eragon. Szczerze mówiąc to nawet ja tak na początku myślałam. Zajrzyjmy teraz do środka. Książka jest dość duża, czyli co oczywiste są większe strony. Do tego czcionka jest dość mała. Czytanie takiej książki nie jest raczej przyjemne.

Podsumowując. Książka nie jest perfekcyjna, jednak warto ją przeczytać. Nawet dla samej wiedzy jaka tak naprawdę jest słynna powieść opowiadająca o losach Eragona. A do tego można trochę się pośmiać kiedy tytuowy chłopak mdleje :).



## Slajd 6 - Podsumowanie

​	Książka jest skierowana do młodszego odbiorcy, co nie oznacza, że starsi nie przeczytają jej z zainteresowaniem.
Opowieść jest bardzo sprawnym połączeniem dziesiątków wątków znanych z książek czy filmów, a tak naprawdę z całej współczesnej kultury popularne.
Trzeba wspomnieć o języku powieści. Jest wyraźnie prostszy od tego, który znamy nawet z Harrego Pottera. Wyraźnie czuć w nim rękę 15 latka, który jeszcze musi popracować nad osiągnięciem charakterystycznego stylu. Nie zmienia to faktu, że język jest wprost idealny dla grupy docelowej, którą jest młodzież. Biorąc jednak poprawkę na młody wiek autora nie można mieć o to pretensji, a raczej trzeba zachwycić się jego umiejętnościami. Po kolejnych tomach należy spodziewać się znacznie więcej, bowiem autor wkroczył już w dorosłe życie. Mam nadzieję, że nie sprawi zawodu czytelnikom oczekującym dalszych przygód Eragona.

![](C:\Users\Ja\zad2\Cykl.jpg)
